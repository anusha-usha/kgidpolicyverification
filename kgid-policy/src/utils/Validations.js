class Validation{
    
    onKeyPressNumberOnly(event){
        const re = /[0-9]+( [0-9]+)*$/g;
        if (!re.test(event.key)) { event.preventDefault(); }
      }
}
export default new Validation;