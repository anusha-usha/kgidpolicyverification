import React from 'react';
import GOKLogo from './../../assets/images/GOK.png'
import {Col,Row} from 'reactstrap';

const LayoutHeader = () => {
    return (
        <div className="layout-header p-2">
            <Row>
                <Col md={4}>
                </Col>
                <Col md={8}>
                    <Row>
                        <Col md={1}>
                            <img src={GOKLogo} className="logo-center mt-2 mb-2" />
                        </Col>
                        <Col md={5} className="col-md-5 text-center mt-2 mb-2">
                        <label><b>Karnataka Government Insurance Department</b></label>
                        <div className="mr-8 text-center">
                            <label><b>Government of Karnataka</b></label>
                        </div>
                        </Col>
                        <Col md={2}>
                            <img src={GOKLogo} className="logo-center mt-2 mb-2" />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )
}

export default LayoutHeader;