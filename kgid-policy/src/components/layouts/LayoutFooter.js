import React from 'react';
import { Col, Row, Container } from 'reactstrap';
import ArrowIcon from '../../assets/images/arrow-icon.png';
import CSGLogo from '../../assets/images/csg-logo.png';

const LayoutFooter = () => {

    return (
        <div className="layout-footer">
            <div className="footer-first-section text-center">
                <Container fluid>
                    <Row style={{ "justifyContent": "center" }}>
                        <ul className="list-inline">
                            <li className="p10">
                                <a>Terms&conditions |</a>
                            </li>
                            <li className="p10">
                                <a>Disclaimer |</a>
                            </li>
                            <li className="p10">
                                <a>privacy and policy |</a>
                            </li>
                            <li className="p10">
                                <a>Copyrights |</a>
                            </li>
                            <li className="p10">
                                <a>Hyperlink policy |</a>
                            </li>


                            <li className="p10">
                                <a>Security policy |</a>
                            </li>

                            <li className="p10">
                                <a>Feedback</a>
                            </li>

                        </ul>
                    </Row>
                </Container>
            </div>
            <div class="footer-second-section">
                <Container>
                    <Row>
                        <Col md={1}>
                        </Col>
                        <Col md={3}>
                            <label><b>Address</b></label>
                            <label><span>
                                <img src={ArrowIcon} />
                            </span>
                                Visvesvaraya Tower, 14th-19th Floor, Dr.B.B. R, Dr Ambedkar Rd, Ambedkar Veedhi, Bengaluru, Karnataka 560001
                            </label>
                        </Col>
                        <Col md={3}>
                        </Col>
                        <Col md={4}>
                            <label>  Designed and Developed by Center for Smart Governance Government of Karnataka V1.0</label>
                        </Col>
                        <Col md={1} style={{'backgroundImage': 'url('+ CSGLogo +')', 'backgroundRepeat':'no-repeat'}}>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>


    )

}

export default LayoutFooter;