import React, {useState} from 'react';
import loginBackgroundImage from '../../assets/images/Backgroundimage.jpg';
import { Col, Row, Card, CardBody, CardHeader, Container,FormGroup, Input,
    Label } from 'reactstrap';
import { Formik, Form } from 'formik';
// import Captcha from "demos-react-captcha";

const LoginPage = () => {
    const [userID, setUserID] = useState('');
    const [otp, setOTP] = useState('');
    const [captcha, setCaptcha] = useState(false);
    const [userIDError, setUserIDError] = useState('')
    const [otpError, setOtpError] = useState('')

  return (
      <div className="login-section" style={{backgroundImage: 'url('+ loginBackgroundImage +')',
      width: '100%',
      height: '75vh',
      backgroundSize: 'cover',
      backgroundPosition: '50% 50%',
      backgroundRepeat: 'no-repeat',
      position:'relative'}}> 
      {/* <Row>
        <Col md={4}></Col>
        <Col md={3} className="mr-2">
        <div class="login-box">
        <Card className="login-card">
            <CardHeader className="border-bottom border-orange">
             <h6 className="text-center"><b>Login</b></h6>
            </CardHeader>
            <CardBody>
              <Container>
                  <Row className="justify-content-left align-items-left">
                    <Col md={8}>
                    <Formik>
                    <Form autoComplete="off">
                    <FormGroup>
                        <Label for="userID">User ID<span style={{color : "red"}}>*</span></Label>
                        <Input
                          type="text"
                          name="userID"
                          id="userID"
                          maxLength="7"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please Enter User ID "
                          required
                        />
                        <p className="message1">  {userIDError} </p>

                      </FormGroup>
                      <FormGroup>
                        <Label for="otp">OTP<span style={{color : "red"}}>*</span></Label>
                        <Input
                          type="text"
                          name="userID"
                          id="userID"
                          maxLength="7"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please Enter OTP"
                          required
                        />
                        <p className="message1">  {otpError} </p>

                      </FormGroup>
                        </Form>
                    </Formik>
                    </Col>
                  </Row>
              </Container>
            </CardBody>
            </Card>
            </div>
        </Col>
        </Row>  */}

        <div className="items-center login-card top-buffer-80">
        <Card>
            <CardHeader className="border-bottom border-orange">
             <h6 className="text-center"><b>Login</b></h6>
            </CardHeader>
            <CardBody>
              <Container>
                  <Row className="justify-content-left align-items-left">
                    <Col md={8}>
                    <Formik>
                    <Form autoComplete="off">
                    <FormGroup>
                        <Label for="userID">User ID<span style={{color : "red"}}>*</span></Label>
                        <Input
                          type="text"
                          name="userID"
                          id="userID"
                          maxLength="7"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please Enter User ID "
                          required
                        />
                        <p className="message1">  {userIDError} </p>

                      </FormGroup>
                      <FormGroup>
                        <Label for="otp">OTP<span style={{color : "red"}}>*</span></Label>
                        <Input
                          type="text"
                          name="userID"
                          id="userID"
                          maxLength="7"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Please Enter OTP"
                          required
                        />
                        <p className="message1">  {otpError} </p>

                      </FormGroup>
                      {/* <FormGroup>
                        <Label for="otp">OTP<span style={{color : "red"}}>*</span></Label>
                        <Input
                          type="text"
                          name="userID"
                          id="userID"
                          maxLength="7"
                          className="form-control"
                          autoComplete="off"
                          placeholder="Enter Captcha"
                          length={6}
                          required
                        />
                        <p className="message1">  {otpError} </p>

                      </FormGroup> */}
                      
                        </Form>
                    </Formik>
                    </Col>
                  </Row>
              </Container>
            </CardBody>
            </Card>
        </div>
      </div>
  )
}

export default LoginPage;