import logo from './logo.svg';
import './App.css';
import LayoutHeader from './components/layouts/LayoutHeader';
import LayoutFooter from './components/layouts/LayoutFooter';
import LoginPage from './components/pages/LoginPage';

function App() {
  return (
    <div>
   <LayoutHeader/>
   <LoginPage/>
   <LayoutFooter/>
   </div>
  );
}

export default App;
